from django.contrib import admin
from django.urls import path, include

from login import views


app_name = 'login'

urlpatterns = [
    path('', views.auth, name="user_auth_page"),
    path('logout/', views.user_logout, name="user_auth_logout"),
]
