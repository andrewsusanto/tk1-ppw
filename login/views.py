from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import logout

# Create your views here.
def auth(request):
    # Default Next URL
    next_url = '/'

    # If POST, save hold data to session
    if request.method == 'POST':
        request.session['next_data'] = request.POST

    # Check if any next url in url parameter
    if 'next' in request.GET:
        next_url = request.GET['next']
        
    context = {
        'next_url' : next_url
    }
    return render(request, 'user_login.html', context=context)

def user_logout(request):
    logout(request)
    return redirect('/')