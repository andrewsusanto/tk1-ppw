from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest

import kupon.views
from kupon.models import Kupon
import datetime

from django.conf import settings

class KuponTest(TestCase):
    def test_kupon_return_name(self):
        kupon = Kupon.objects.create(
            nama_kupon = 'diskon',
            jenis_kupon = 'diskon50',
            sk_singkat_kupon = 'Sampai 20 Januari',
            sk_kupon = 'SK PANJANG',
            kode = 'DISKON20',
            persen_diskon = 10,
            harga_minimum = 10000,
            tanggal_expired_kupon = datetime.datetime.now() + datetime.timedelta(days=1)
        )
        self.assertEqual(str(kupon), 'diskon50')

    def test_kupon_page(self):
        kupon = Kupon.objects.create(
            nama_kupon = 'diskon',
            jenis_kupon = 'diskon50',
            sk_singkat_kupon = 'Sampai 20 Januari',
            sk_kupon = 'SK PANJANG',
            kode = 'DISKON20',
            persen_diskon = 10,
            harga_minimum = 10000,
            tanggal_expired_kupon = datetime.datetime.now() + datetime.timedelta(days=1)
        )
        response = Client().get('/coupon/')
        self.assertEqual(response.status_code, 200)