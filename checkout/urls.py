from django.urls import path, include
from . import views

app_name = 'checkout'

urlpatterns = [
    path('',views.index, name='checkoutpage'),
    path('process', views.process, name="process"),
    path('complete/<int:pk>', views.done, name="checkoutcomplete"),
    path('api/v1/searchcoupon/', views.search_coupon, name="searchcoupon"),
]
