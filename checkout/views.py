from django.shortcuts import render, redirect, reverse
from django.http import JsonResponse, HttpResponseForbidden
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
import datetime
import time
import json

from storeadmin.models import Barang
from kupon.models import Kupon
from checkout.models import Transaksi, Item
from checkout.utils import money_format

def index(request):
    if(not request.user.is_authenticated):
        request.session['next_data'] = request.POST
        return redirect('/auth/' + '?next=/checkout/')

    barang_id = None
    if request.session.get('next_data'):
        if 'barang-id' in request.session.get('next_data'):
            barang_id = request.session['next_data']['barang-id']
            del request.session['next_data']

    if 'barang-id' in request.POST:
        barang_id = request.POST['barang-id']

    if barang_id:
        try:
            barang = Barang.objects.get(id=barang_id)
        except:
            return redirect('/')

        if barang.stok > 0:
            kupon = Kupon.objects.filter(tanggal_expired_kupon__gte = datetime.datetime.now(), harga_minimum__lte = barang.harga)
            barang.harga =  money_format(barang.harga)

            context = {
                'barang': barang,
                'kupon': kupon,
                'total_harga': barang.harga
            }
            return render(request, 'checkout.html', context)

        else:
            return redirect('/')

    else:
        item_list = Item.objects.filter(user=request.user, transaksi=None)
        if len(item_list) == 0:
            return redirect('/')

        total_price = 0
        for item in  item_list:
            total_price += item.total_harga
            item.barang.harga = money_format(item.barang.harga)

        kupon = Kupon.objects.filter(tanggal_expired_kupon__gte = datetime.datetime.now(), harga_minimum__lte = total_price)

        context = {
            'item_list': item_list,
            'kupon': kupon,
            'total_harga': money_format(total_price),
        }
        return render(request, 'checkout.html', context)

@login_required(login_url='login:user_auth_page')
def process(request):
    if request.is_ajax():
        time.sleep(1)
        response = {}
        if all(post_data in request.POST for post_data in ['payment-method', 'coupon-id']):
            data = request.POST

            if data['coupon-id']:
                kupon = Kupon.objects.get(id=data['coupon-id'])
            else:
                kupon = None
            
            if('barang-id' in data and data['barang-id']):
                try:
                    barang = Barang.objects.get(id=data['barang-id'])
                    transaksi = Transaksi.objects.create(
                        user=request.user,
                        kupon=kupon,
                        is_processed=True,
                        payment_method=data['payment-method'],
                    )
                    item = Item.objects.create(
                        user=request.user,
                        transaksi=transaksi,
                        barang=barang,
                        jumlah=1,
                    )
                    transaksi.save()
                except:
                    response['success'] = False
    
            else:
                item_list = Item.objects.filter(user=request.user, transaksi=None)
                if len(item_list) > 0 and all(item.jumlah <= item.barang.stok for item in item_list):
                    transaksi = Transaksi.objects.create(
                        user=request.user,
                        kupon=kupon,
                        is_processed=True,
                        payment_method=data['payment-method']
                    )
                    for item in item_list:
                        item.transaksi = transaksi
                        item.save()
                    
                    transaksi.save()

                else:
                    response['success'] = False

        else:
            response['success'] = False

        if 'success' not in response:
            response['success'] = True
            response['transaksi_id'] = transaksi.id

        return JsonResponse(response)
    
    else:
        return HttpResponseForbidden()

@login_required(login_url='login:user_auth_page')
def done(request, pk):
    try:
        transaksi = Transaksi.objects.get(id=pk, user=request.user)

    except:
        return redirect('/')

    order_total = money_format(transaksi.total_transaksi + transaksi.total_diskon)
    total_transaksi = money_format(transaksi.total_transaksi)
    totaldiskon = money_format(transaksi.total_diskon)

    item_list = transaksi.item_set.all()
    for item in item_list:
        item.barang.harga = money_format(item.barang.harga)

    return render(request, 'checkoutdone.html', {
        'item_list': item_list,
        'kupon': transaksi.kupon,
        'ordertotal': order_total,
        'totaltransaksi': total_transaksi,
        'nama' : request.user.first_name,
        'paymentmethod' : transaksi.payment_method,
        'totaldiskon': totaldiskon,
    })

def search_coupon(request):
    kupon = Kupon.objects.filter(tanggal_expired_kupon__gte = datetime.datetime.now())
    data_kupon = []
    for kupon_item in kupon:
        data_kupon.append({
            'nama_kupon' : kupon_item.nama_kupon,
            'id' : kupon_item.id,
            'gambar' : kupon_item.gambar,
            'jenis_kupon' : kupon_item.jenis_kupon,
            'sk_singkat_kupon' : kupon_item.sk_singkat_kupon,
            'sk_kupon' : kupon_item.sk_kupon,
            'kode' : kupon_item.kode,
            'persen_diskon' : kupon_item.persen_diskon,
            'harga_minimum' : kupon_item.harga_minimum,
            'tanggal_expired_kupon' : kupon_item.tanggal_expired_kupon,
        })
    json_data = {
        'kupon' : data_kupon
    }
    return JsonResponse(json_data, safe=False)
