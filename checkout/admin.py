from django.contrib import admin
from checkout.models import Transaksi, Item

# Register your models here.
admin.site.register(Transaksi)
admin.site.register(Item)