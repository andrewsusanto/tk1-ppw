from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
import datetime
from django.contrib.auth.models import User
import os
import time

from checkout.models import *
from storeadmin.models import *
from kupon.models import *
from landingpage.models import *
from checkout.views import index
from checkout.utils import money_format

class CheckoutTest(TestCase) :
    
    def setUp(self):
        super().setUp()
        self.kategori_test = Kategori.objects.create(nama="kategori_test")
        self.barang_test = Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=self.kategori_test,
        )
        self.barang_mainan = Barang.objects.create(
            nama="mainan_test",
            harga=50000,
            stok=100,
            deskripsi="toy story",
            kategori=self.kategori_test,
        )
        self.kupon_test = Kupon.objects.create(
            nama_kupon="kupon_test",
            jenis_kupon="kupon_test",
            sk_kupon="apa",
            sk_singkat_kupon = "apa",
            kode="kode_test",
            persen_diskon=50,
            harga_minimum=1000,
            tanggal_expired_kupon=datetime.datetime.now()+datetime.timedelta(days=1),
        )
        self.user_test = User.objects.create_user(
            username="user_test",
            email="user@user.com",
            password="user_test",
        )

    def test_checkout_is_exist(self) :
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_checkout_model_create(self) :        
        transaksi = Transaksi.objects.create(
            user=self.user_test,
            kupon=self.kupon_test,
        )

        jumlah = Transaksi.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_checkout_get_redirect(self):
        response = Client().get('/checkout/')
        self.assertEqual(response.status_code, 302)

    def test_checkout_post_to_process(self):
        self.client.force_login(self.user_test)
        data = {}
        response = self.client.post('/checkout/process', data=data)
        self.assertEqual(response.status_code, 403)

    def test_checkout_post_item_found(self):
        self.client.force_login(self.user_test)
        data = {
            'barang-id': self.barang_test.id
        }
        response = self.client.post('/checkout/', data=data)
        self.assertEqual(response.status_code, 200)

    def test_checkout_wrong_item_id(self):
        self.client.force_login(self.user_test)
        data = {
            'barang-id': 500,
        }
        response = self.client.post('/checkout/', data=data)
        self.assertEqual(response.status_code, 302)
    
    def test_checkout_no_item(self):
        self.client.force_login(self.user_test)
        response = self.client.post('/checkout/')
        self.assertEqual(response.status_code, 302)

    def test_checkout_post_item_zero_stock(self):
        self.client.force_login(self.user_test)
        self.barang_test.stok = 0
        self.barang_test.save()
        data = {
            'barang-id': self.barang_test.id
        }
        response = self.client.post('/checkout/', data=data)
        self.assertEqual(response.status_code,302)

    def test_checkout_make_transaction_without_coupon(self):
        transaksi = Transaksi.objects.create(
            user=self.user_test,
        )
        barang_dibeli = Item.objects.create(
            user=self.user_test,
            transaksi=transaksi,
            barang=self.barang_test,
            jumlah=1,
        )
        self.client.force_login(self.user_test)
        response = self.client.get('/checkout/complete/1')
        self.assertEqual(response.status_code, 200)

    def test_checkout_make_transaction_with_coupon(self):
        transaksi = Transaksi.objects.create(
            user=self.user_test,
            kupon=self.kupon_test,
        )
        barang_dibeli = Item.objects.create(
            user=self.user_test,
            transaksi=transaksi,
            barang=self.barang_test,
            jumlah=1,
        )
        transaksi.save()
        self.client.force_login(self.user_test)
        response = self.client.get('/checkout/complete/1')
        self.assertEqual(response.status_code,200)
    
    def test_checkout_other_customer_access_other_customers_transaction(self):
        transaksi = Transaksi.objects.create(
            user=self.user_test,
            kupon=self.kupon_test,
        )
        other_user = User.objects.create_user('orang', 'orang@orang.com', 'orang')
        self.client.force_login(other_user)
        response = self.client.get('/checkout/complete/1')
        self.assertEqual(response.status_code, 302)

    def test_checkout_buy_now_before_login(self):
        data = {
            'barang-id': 1,
        }
        self.client.post('/checkout/', data=data)
        self.client.force_login(self.user_test)
        response = self.client.get('/checkout/')
        self.assertEqual(response.status_code, 200)

    def test_checkout_many_items_access_checkout_page(self):
        self.client.force_login(self.user_test)
        to_buy1 = Item.objects.create(
            user=self.user_test,
            barang=self.barang_test,
            jumlah=2,
        )
        to_buy2 = Item.objects.create(
            user=self.user_test,
            barang=self.barang_mainan,
            jumlah=3,
        )
        response = self.client.get('/checkout/')
        self.assertEqual(response.status_code, 200)

    def test_checkout_many_items_displayed_in_checkout_page(self):
        self.client.force_login(self.user_test)
        item1 = Item.objects.create(
            user=self.user_test,
            barang=self.barang_test,
            jumlah=1
        )
        item2 = Item.objects.create(
            user=self.user_test,
            barang=self.barang_mainan,
            jumlah=1
        )

        response = self.client.get('/checkout/')
        page_source = response.content.decode('utf8')
        self.assertEqual(page_source.count('<div class="checkout-item">'), 2)

    def test_checkout_many_items_displayed_in_checkout_page_with_prior_transaction(self):
        self.client.force_login(self.user_test)
        prior_transaction = Transaksi.objects.create(
            user=self.user_test,
            is_processed=True,
        )
        prior_item = Item.objects.create(
            user=self.user_test,
            transaksi=prior_transaction,
            barang=self.barang_test,
            jumlah=1,
        )
        item1 = Item.objects.create(
            user=self.user_test,
            barang=self.barang_test,
            jumlah=1
        )
        item2 = Item.objects.create(
            user=self.user_test,
            barang=self.barang_mainan,
            jumlah=1
        )

        response = self.client.get('/checkout/')
        page_source = response.content.decode('utf8')
        self.assertEqual(page_source.count('<div class="checkout-item">'), 2)

    def test_checkout_total_price_displayed(self):
        self.client.force_login(self.user_test)
        item1 = Item.objects.create(
            user=self.user_test,
            barang=self.barang_test,
            jumlah=2
        )
        item2 = Item.objects.create(
            user=self.user_test,
            barang=self.barang_mainan,
            jumlah=2
        )
        total_harga = item1.barang.harga*item1.jumlah + item2.barang.harga*item2.jumlah
        response = self.client.get('/checkout/')
        page_source = response.content.decode('utf8')
        self.assertIn(money_format(total_harga), page_source)

class CheckoutFunctionalTest(LiveServerTestCase) :
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)   

        self.user_ajo = User.objects.create_user(
            username="ajo",
            email="ajo@ajo.com",
            password="ajo",
        )
        self.kategori_test = Kategori.objects.create(nama="kategori_test")
        self.barang_test = Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=self.kategori_test,
        )
        self.barang_mainan = Barang.objects.create(
            nama="mainan_test",
            harga=50000,
            stok=100,
            deskripsi="toy story",
            kategori=self.kategori_test,
        )

        super(CheckoutFunctionalTest, self).setUp()     

    def tearDown(self):
        self.browser.quit()
        super(CheckoutFunctionalTest, self).tearDown()

    def test_searchbox_exist_in_modal_and_search_for_coupon(self):
        kategori = Kategori.objects.create(nama="makanan")
        Barang.objects.create(
            nama="Rendang",
            harga=10000,
            stok=50,
            deskripsi="Makanan Padang",
            kategori=kategori,
        )

        test_kupon = Kupon.objects.create(
            nama_kupon="diskon_50",
            jenis_kupon="kupon_test",
            sk_kupon="apa",
            sk_singkat_kupon = "apa",
            kode="kode_test",
            persen_diskon=50,
            harga_minimum=1000,
            tanggal_expired_kupon=datetime.datetime.now()+datetime.timedelta(days=1)
        )
        
        self.browser.get(self.live_server_url + '/auth')
        self.browser.implicitly_wait(10)
        email_input = self.browser.find_element_by_xpath("//input[@name='email']")
        password_input = self.browser.find_element_by_xpath("//input[@name='password']")

        email_input.send_keys("ajo@ajo.com")
        password_input.send_keys("ajo")

        login_button = self.browser.find_element_by_class_name('login-button')
        login_button.click()
        time.sleep(2)

        self.browser.get(self.live_server_url + '/')
        product = self.browser.find_element_by_class_name("landingpage-product-a")
        product.click()

        time.sleep(2)

        buynow_button = self.browser.find_element_by_id("buynow")
        buynow_button.click()

        time.sleep(5)
        
        payment_method = self.browser.find_element_by_class_name('payment-selection')
        payment_method.click()

        add_coupon_button = self.browser.find_element_by_class_name("checkout-coupon-add")
        add_coupon_button.click()

        time.sleep(2)

        modal_text = self.browser.find_element_by_class_name("modal-content").text
        self.assertIn('Search coupon', modal_text)

        query = self.browser.find_element_by_id("couponQuery")
        query.send_keys("diskon")

        submit = self.browser.find_element_by_id("search")
        submit.click()

        time.sleep(2)

        modal_text = self.browser.find_element_by_class_name("modal-content").text
        self.assertIn('diskon_50', modal_text)
        time.sleep(0.3)

        coupon_selection = self.browser.find_element_by_class_name('coupon-list-image')
        coupon_selection.click()
        time.sleep(2)

        checkout_button = self.browser.find_element_by_id('checkoutButton')
        checkout_button.click()
        time.sleep(0.3)

        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn("Your transaction is being processed", page_text)

        time.sleep(5)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn("Your transaction is completed", page_text)

    def test_make_transaction_with_many_items_on_cart(self):
        self.browser.get(self.live_server_url + '/auth')
        self.browser.implicitly_wait(10)
        email_input = self.browser.find_element_by_xpath("//input[@name='email']")
        password_input = self.browser.find_element_by_xpath("//input[@name='password']")

        email_input.send_keys("ajo@ajo.com")
        password_input.send_keys("ajo")

        login_button = self.browser.find_element_by_class_name('login-button')
        login_button.click()
        time.sleep(2)

        test_kupon = Kupon.objects.create(
            nama_kupon="diskon_50",
            jenis_kupon="kupon_test",
            sk_kupon="apa",
            sk_singkat_kupon = "apa",
            kode="kode_test",
            persen_diskon=50,
            harga_minimum=1000,
            tanggal_expired_kupon=datetime.datetime.now()+datetime.timedelta(days=1)
        )
        item1 = Item.objects.create(
            user=self.user_ajo,
            barang=self.barang_test,
            jumlah=3,
        )
        item2 = Item.objects.create(
            user=self.user_ajo,
            barang=self.barang_mainan,
            jumlah=2,
        )

        self.browser.get(self.live_server_url + "/checkout/")
        time.sleep(2)

        payment_method = self.browser.find_element_by_class_name('payment-selection')
        payment_method.click()

        add_coupon_button = self.browser.find_element_by_class_name("checkout-coupon-add")
        add_coupon_button.click()
        time.sleep(2)

        coupon_selection = self.browser.find_element_by_class_name('coupon-list-image')
        coupon_selection.click()
        time.sleep(2)

        total_price = item1.total_harga + item2.total_harga
        discount = int(total_price * test_kupon.persen_diskon / 100)
        price_after_discount = total_price - discount

        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn(money_format(total_price), page_text)
        self.assertIn(money_format(discount), page_text)
        self.assertIn(money_format(price_after_discount), page_text)

        coupon_remove_button = self.browser.find_element_by_id('couponRemoveButton')
        coupon_remove_button.click()
        time.sleep(1)

        html_total_payment = self.browser.find_element_by_id('totalPayment').text
        self.assertIn(money_format(total_price), html_total_payment)

        checkout_button = self.browser.find_element_by_id('checkoutButton')
        checkout_button.click()
        time.sleep(0.3)

        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn("Your transaction is being processed", page_text)

        time.sleep(5)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn("Your transaction is completed", page_text)

        process_button = self.browser.find_element_by_id('processButton')
        process_button.click()

        transaksi_count = Transaksi.objects.all().count()
        self.assertEqual(transaksi_count, 1)

        total_paid = item1.total_harga + item2.total_harga
        page_text = self.browser.find_element_by_tag_name('body').text

        self.assertIn(money_format(total_paid), page_text)
