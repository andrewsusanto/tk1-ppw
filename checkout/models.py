from django.db import models
from django.core.validators import *
from django.contrib.auth.models import User

from storeadmin.models import Barang
from kupon.models import Kupon
from checkout.utils import money_format


class Transaksi(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    kupon = models.ForeignKey(Kupon, on_delete=models.CASCADE, blank=True, null=True)
    payment_method = models.CharField(max_length=255)
    total_transaksi = models.IntegerField(default=0)
    total_diskon = models.IntegerField(default=0)
    is_processed = models.BooleanField(default=False)
    tanggal_beli = models.DateTimeField(auto_now=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        for item in self.item_set.all():
            self.total_transaksi += item.total_harga
            item.barang.terjual += item.jumlah
            item.barang.stok -= item.jumlah
            item.barang.save()

        if self.kupon:
            self.total_diskon = int(self.total_transaksi * self.kupon.persen_diskon / 100)
            self.total_transaksi -= self.total_diskon

        return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    def items(self):
        item_set = self.item_set.all()
        for item in item_set:
            item.barang.harga = money_format(item.barang.harga)

        return item_set

    def __str__(self):
        return f"[{self.tanggal_beli}] {self.user.username} - {self.total_transaksi}"

class Item(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    transaksi = models.ForeignKey(Transaksi, on_delete=models.CASCADE, blank=True, null=True)
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE)
    jumlah = models.IntegerField(validators=[MinValueValidator(1)])
    total_harga = models.IntegerField(default=0, validators=[MinValueValidator(0)])

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.total_harga = self.jumlah * self.barang.harga
        
        return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)
    
    def total(self):
        return f'{self.total_harga:,}'.replace(',','.')
        
    def __str__(self):
        status = "NULL" if not (self.transaksi and self.transaksi.is_processed) else f"{self.transaksi.tanggal_beli}"
        return f"[{status}] {self.barang} - {self.jumlah}"
