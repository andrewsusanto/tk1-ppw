from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.conf import settings
from importlib import import_module
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session

from importlib import import_module
from django.conf import settings as django_settings

from storeadmin.models import *
from landingpage.models import *
import storeadmin.views 


class StoreAdminUnitTest(TestCase) :

    def setUp(self):
        self.user = User.objects.create_superuser("admin", "admin@admin.com", "admin")

    def test_storeadmin_url_is_exist_not_logged_in (self) :
        response = Client().get('/admin/login')
        self.assertEqual(response.status_code, 200)

        response = Client().get('/admin/')
        self.assertEqual(response.status_code, 302)
        
        response = Client().get('/admin/manage/add')
        self.assertEqual(response.status_code, 302)

        response = Client().get('/admin/manage')
        self.assertEqual(response.status_code, 302)

    def test_storeadmin_url_is_exist_logged_in (self) :
        logged_in = self.client.force_login(user=self.user)
        response = self.client.get("/admin/login")
        self.assertEqual(response.status_code, 302)
        
        response = self.client.get('/admin/manage/add')
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/admin/manage')
        self.assertEqual(response.status_code, 200)

    def test_storeadmin_wrong_login_post (self) :
        response = Client().post("/admin/login", data={
            "username" : "admin",
            "password" : "salahin"
        })
        html_response = response.content.decode('utf8')
        self.assertIn("Invalid Username or Password", html_response)

    def test_storeadmin_login_post (self) :
        response = Client().post("/admin/login", data={
            "username" : "admin",
            "password" : "admin"
        })
        self.assertURLEqual(response.url, "/admin/manage")

    def test_storeadmin_model_create (self) :
        Kategori.objects.create(nama="test_kategori")
        kategori = Kategori.objects.get(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )
        logged_in = self.client.force_login(user=self.user)

        response = self.client.get("/admin/")
        html_response = response.content.decode('utf8')
        self.assertIn("barang_test", html_response)

    def test_storeadmin_model_delete (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )
        logged_in = self.client.force_login(user=self.user)

        response = self.client.get("/admin/manage/delete/1")
        count = Barang.objects.all().count()
        self.assertEqual(count, 0)

    def test_storeadmin_model_delete_photo_not_in_dropbox (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
            foto="/static/logo.png"
        )

        barang.delete()
        count = Barang.objects.all().count()
        self.assertEqual(count, 0)

    def test_storeadmin_model_update (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )
        logged_in = self.client.force_login(user=self.user)

        response = self.client.post("/admin/manage/update/1", data={
            "nama" : "nama_baru",
            "stok" : 3,
            "harga" : 123,
            "deskripsi" : "deskripsi_baru",
            "kategori" : kategori,
        })
        response = self.client.get("/admin/manage")
        html_response = response.content.decode('utf8')
        self.assertIn("nama_baru", html_response)

    def test_storeadmin_model_create_post (self) :
        logged_in = self.client.force_login(user=self.user)

        kategori = Kategori.objects.create(nama="test_kategori")
        response = self.client.post("/admin/manage/add", data={
            "nama" : "barang_baru",
            "harga" : 123,
            "stok" : 3,
            "deskripsi" : "deskripsi_baru",
            "kategori" : kategori.id,
        })
        count = Barang.objects.all().count()
        self.assertEqual(count, 1)

    def test_storeadmin_non_superuser_access_admin_page(self):
        non_superuser = User.objects.create_user(
            username="normies",
            email="normies@normie.com",
            password="normies_ftw"
        )
        self.client.force_login(user=non_superuser)

        response = self.client.get('/admin/manage')
        self.assertEquals(response.status_code, 302)

        response = self.client.get('/admin/manage/add')
        self.assertEquals(response.status_code, 302)
