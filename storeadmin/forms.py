from django import forms  
from .models import Barang

class BarangEditForm(forms.ModelForm) :
    class Meta :
        model = Barang
        fields = '__all__'
        widgets = {
            'nama': forms.TextInput(attrs={
                'placeholder': 'Masukkan nama produk'
            }),
            'stok': forms.NumberInput(attrs={
                'placeholder': 'Masukkan jumlah stok barang'
            }),
            'harga': forms.NumberInput(attrs={
                'placeholder': 'Masukkan harga produk'
            }),
            'deskripsi': forms.Textarea(attrs={
                'placeholder': 'Masukkan deskripsi produk...'
            }),
        }