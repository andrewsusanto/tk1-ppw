from django.db import models
from landingpage.models import *
from django.core.validators import *
from dropbox.dropbox import Dropbox

from django.db.models.signals import post_delete
from django.dispatch import receiver

import os

class Barang(models.Model):
    nama = models.CharField(max_length = 64)
    harga = models.IntegerField(validators=[MinValueValidator(0)])
    stok = models.IntegerField(validators=[MinValueValidator(0)])
    terjual = models.IntegerField(default=0, validators=[MinValueValidator(0)], blank=True)
    deskripsi = models.TextField()
    foto = models.ImageField(upload_to="", blank=True)
    link_foto = models.CharField(max_length=1024, default="", blank=True)
    kategori = models.ForeignKey(Kategori, null=True, on_delete=models.SET_NULL)
    
    def __str__(self):
        return self.nama

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        try:
            dbx = Dropbox("omMNqUb0sSAAAAAAAAAADt4I9h-3pLeZLUFDag1X2ng5gxC9y7z-PnCbHMkciTCA")
            self.link_foto = dbx.sharing_create_shared_link(f"/{self.foto}").url[:-4] + "raw=1"
        except:
            pass

        return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

@receiver(post_delete, sender=Barang)
def submission_delete(sender, instance, **kwargs) :
    try :
        instance.foto.delete(False)
    except :
        # If foto doesnt exist in Dropbox
        pass