from django.db import models

# Create your models here.
class Kategori(models.Model):
    nama = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return self.nama