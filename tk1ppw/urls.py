"""tk1ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from detailpage.views import cart, api_add_to_cart, api_remove_from_cart, api_mengurangi_jumlah_barang, api_menambahi_jumlah_barang

urlpatterns = [
    path('db-admin/', admin.site.urls),
    path('transaction/', include('historypage.urls')),
    path('coupon/', include('kupon.urls')),
    path('checkout/', include('checkout.urls')),
    path('admin/', include('storeadmin.urls')),
    path('detail/', include('detailpage.urls')),
    path('auth/', include('login.urls')),
    path('api/v1/auth/', include('authapi.urls')),
    path('', include('landingpage.urls')),
    path('cart/', cart, name='cart'),
    path('tambah/<int:id_barang>/', api_add_to_cart, name='addToCart'),
    path('hapus/<int:id_barang>/', api_remove_from_cart, name="removeFromCart"),
    path('kurangin/<int:id_barang>/', api_mengurangi_jumlah_barang, name="kurangiBarang"),
    path('tambahin/<int:id_barang>/', api_menambahi_jumlah_barang, name="tambahinBarang"),
]
