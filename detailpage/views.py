from django.shortcuts import render, HttpResponseRedirect, redirect
from django.urls import reverse
from django.views.generic import CreateView
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from storeadmin.models import Barang
from checkout.models import Transaksi, Item
from detailpage.models import Ulasan


def index(request, pk):
    if request.method == 'POST':
        barang = Barang.objects.get(id=pk)
        nama = request.POST['nama']
        isi = request.POST['isi']
        nilai = request.POST['rating']
        ulasanObj = Ulasan(nama_pengulas=nama,
                           isi_ulasan=isi, barang=barang, penilaian=nilai)
        ulasanObj.save()  
    barang = Barang.objects.filter(id=pk)
    if barang.count() == 0:
        return redirect('/')
    barang = barang[0]
    count = barang.terjual
    ulasan = Ulasan.objects.filter(barang = barang)
    ulasan_score = 0
    if ulasan.count() != 0:
        for x in ulasan:
            ulasan_score += x.penilaian
        ulasan_score /= ulasan.count()
    else:
        ulasan_count = 0
    ulasan_score = round(ulasan_score,1)
    ulasan_star = range(round(ulasan_score))
    ulasan_no_star = range(5-round(ulasan_score))
    jumlah_ulasan = ulasan.count()
    ulasan_with_star = []

    # Format Price
    barang.harga = f'{barang.harga:,}'.replace(',','.')

    # Remove leading and trailing whitespace on description
    barang.deskripsi = barang.deskripsi.strip()
    
    for x in ulasan:
        ulasan_with_star.append((x,range(x.penilaian), range(5-x.penilaian)))

    context = {
        'Ulasan': ulasan_with_star,
        'Barang': barang,
        'Sold' : count,
        'Star' : ulasan_star,
        'NoStar' : ulasan_no_star,
        'UlasanScore' : ulasan_score,
        'UlasanCount' : jumlah_ulasan
    }
    return render(request, 'detailpage.html', context)

def no_id(request):
    return redirect('/')

def cart(request):
    items = Item.objects.all().filter(user=request.user, transaksi=None)
    total_harga = 0
    for item in items:
        total_harga += item.total_harga
    context = {
        'itemList' : items,
        'totalHarga' : total_harga,
    }
    return render(request, 'cart.html', context)

@csrf_exempt
def api_add_to_cart(request, id_barang):
    if request.user.is_authenticated and request.method == 'POST' and request.is_ajax():
        data = json.loads(request.body)
        try:
            barang = Barang.objects.get(id=data['barang_id'])
        except:
            return JsonResponse({
                'status' : 404
            })
        
        try:
            item = Item.objects.get(user=request.user, barang=barang, transaksi=None)
            item.jumlah += 1
            item.save()
        except Item.DoesNotExist:
            Item.objects.create(
                user=request.user,
                barang=barang,
                transaksi=None,
                jumlah=1,
            )
        return JsonResponse({
            'status':200
        })

@csrf_exempt
def api_remove_from_cart(request, id_barang):
    if request.user.is_authenticated and request.method =='POST' and request.is_ajax():
        try:
            barang = Barang.objects.get(id=id_barang)
        except:
            return JsonResponse({
                'status':404
            })
        try:
            item = Item.objects.get(user=request.user, barang=barang, transaksi=None)
            item.delete()
        except Item.DoesNotExist:
            return JsonResponse({
                'status':404
            })
        return JsonResponse({
            'status':200
        })

@csrf_exempt
def api_mengurangi_jumlah_barang(request, id_barang):
    if request.user.is_authenticated and request.method =='POST' and request.is_ajax():
        try:
            barang = Barang.objects.get(id=id_barang)
        except:
            return JsonResponse({
                'status':404
            })
        try:
            item = Item.objects.get(user=request.user, barang=barang, transaksi=None)
            item.jumlah -=1
            if item.jumlah < 1:
                item.delete()
            else:
                item.save()
        except Item.DoesNotExist:
            return JsonResponse({
                'status':404
            })
        return JsonResponse({
            'status':200
        })

@csrf_exempt
def api_menambahi_jumlah_barang(request, id_barang):
    if request.user.is_authenticated and request.method =='POST' and request.is_ajax():
        try:
            barang = Barang.objects.get(id=id_barang)
        except:
            return JsonResponse({
                'status':404
            })
        try:
            item = Item.objects.get(user=request.user, barang=barang, transaksi=None)
            item.jumlah +=1
            item.save()
        except Item.DoesNotExist:
            return JsonResponse({
                'status':404
            })
        return JsonResponse({
            'status':200
        })