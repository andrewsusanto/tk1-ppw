from django.urls import path, include
from . import views
# from .views import UlasanCreateView

urlpatterns = [
    path('<int:pk>', views.index, name='detailpage'),
    path('', views.no_id, name="no id redirect"),
]
