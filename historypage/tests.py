from django.test import TestCase, LiveServerTestCase
from django.test import Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
import datetime
import os
import time

from checkout.models import *
from storeadmin.models import *
from kupon.models import *
from landingpage.models import *
import historypage.views

class HistoryPageUnitTest(TestCase):
    def setUp(self):
        super().setUp()
        self.kategori_test = Kategori.objects.create(nama="kategori_test")
        self.barang_test = Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=self.kategori_test,
        )
        self.barang_mainan = Barang.objects.create(
            nama="mainan_test",
            harga=50000,
            stok=100,
            deskripsi="toy story",
            kategori=self.kategori_test,
        )
        self.kupon_test = Kupon.objects.create(
            nama_kupon="kupon_test",
            jenis_kupon="kupon_test",
            sk_kupon="apa",
            sk_singkat_kupon = "apa",
            kode="kode_test",
            persen_diskon=50,
            harga_minimum=1000,
            tanggal_expired_kupon=datetime.datetime.now()+datetime.timedelta(days=1),
        )
        self.user_test = User.objects.create_user(
            username="user_test",
            email="user@user.com",
            password="user_test",
        )

    def test_history_page_access_without_login(self):
        response = Client().get('/transaction/')
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/auth/'))

    def test_history_page_access_with_login(self):
        self.client.force_login(self.user_test)
        response = self.client.get('/transaction/')
        self.assertEqual(response.status_code, 200)

    def test_history_page_display_nothing(self):
        self.client.force_login(self.user_test)
        response = self.client.get('/transaction/')
        html_page = response.content.decode('utf8')
        self.assertEqual(html_page.count('<div class="transaction-list">'), 0)

    def test_history_page_display_processed_transaction(self):
        self.client.force_login(self.user_test)
        prior_transaction = Transaksi.objects.create(
            user=self.user_test,
            is_processed=True,
        )
        prior_item = Item.objects.create(
            user=self.user_test,
            transaksi=prior_transaction,
            barang=self.barang_test,
            jumlah=1,
        )
        prior_transaction.save()

        response = self.client.get('/transaction/')
        html_page = response.content.decode('utf8')
        self.assertEqual(html_page.count('<div class="transaction-list">'), 1)

    def test_history_page_only_display_own_transaction(self):
        other_user = User.objects.create_user(
            username="other_user",
            password="other_user",
        )
        other_transaction = Transaksi.objects.create(
            user=other_user,
            is_processed=True,
        )
        other_item = Item.objects.create(
            user=other_user,
            transaksi=other_transaction,
            barang=self.barang_test,
            jumlah=1,
        )
        other_transaction.save()

        self.client.force_login(self.user_test)
        prior_transaction = Transaksi.objects.create(
            user=self.user_test,
            is_processed=True,
        )
        prior_item = Item.objects.create(
            user=self.user_test,
            transaksi=prior_transaction,
            barang=self.barang_test,
            jumlah=1,
        )
        prior_transaction.save()

        response = self.client.get('/transaction/')
        html_page = response.content.decode('utf8')
        self.assertEqual(html_page.count('<div class="transaction-list">'), 1)

class CheckoutFunctionalTest(LiveServerTestCase) :
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)   

        self.user_ajo = User.objects.create_user(
            username="ajo",
            email="ajo@ajo.com",
            password="ajo",
        )
        self.kategori_test = Kategori.objects.create(nama="kategori_test")
        self.barang_test = Barang.objects.create(
            nama="barang_test",
            harga=1234,
            stok=1234,
            deskripsi="test_test",
            kategori=self.kategori_test,
        )
        self.barang_mainan = Barang.objects.create(
            nama="mainan_test",
            harga=50000,
            stok=100,
            deskripsi="toy story",
            kategori=self.kategori_test,
        )

        super(CheckoutFunctionalTest, self).setUp()     

    def tearDown(self):
        self.browser.quit()
        super(CheckoutFunctionalTest, self).tearDown()

    def test_access_detail_transactio(self):
        self.browser.get(self.live_server_url + '/auth')
        self.browser.implicitly_wait(10)
        email_input = self.browser.find_element_by_xpath("//input[@name='email']")
        password_input = self.browser.find_element_by_xpath("//input[@name='password']")

        email_input.send_keys("ajo@ajo.com")
        password_input.send_keys("ajo")

        login_button = self.browser.find_element_by_class_name('login-button')
        login_button.click()
        time.sleep(2)

        item1 = Item.objects.create(
            user=self.user_ajo,
            barang=self.barang_test,
            jumlah=3,
        )
        item2 = Item.objects.create(
            user=self.user_ajo,
            barang=self.barang_mainan,
            jumlah=2,
        )

        self.browser.get(self.live_server_url + "/checkout/")
        time.sleep(2)

        payment_method = self.browser.find_element_by_class_name('payment-selection')
        payment_method.click()

        checkout_button = self.browser.find_element_by_id('checkoutButton')
        checkout_button.click()
        time.sleep(3)

        process_button = self.browser.find_element_by_id('processButton')
        process_button.click()

        self.browser.get(self.live_server_url + '/transaction/')
        transaction = self.browser.find_element_by_class_name('transaction-list')
        transaction.click()

        print(self.browser.current_url)
        self.assertIn('/checkout/complete/', self.browser.current_url)
