from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from checkout.models import Transaksi
from checkout.utils import money_format


@login_required(login_url='login:user_auth_page')
def index(request):
    transaksi = Transaksi.objects.filter(user=request.user, is_processed=True).order_by('-tanggal_beli')
    for x in range(len(transaksi)):
        transaksi[x].total_transaksi = money_format(transaksi[x].total_transaksi)
        
    return render(request, 'history.html' , {'transaksi' : transaksi})
