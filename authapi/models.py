from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class GoogleUserData(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    google_id = models.CharField(max_length = 256, unique=True)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name + " (Google Account)"
