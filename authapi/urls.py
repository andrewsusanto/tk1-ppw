from django.contrib import admin
from django.urls import path, include

from authapi import views

urlpatterns = [
    path('login/', views.login_user, name="api_user_login"),
    path('signup/', views.signup, name="api_user_signup"),
    path('g_login/', views.g_login, name="api_google_login"),
]
