from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt

from authapi.models import GoogleUserData

from google.oauth2 import id_token
from google.auth.transport import requests
import json


# Create your views here.
@csrf_exempt
def login_user(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        if all(k in data for k in ['email', 'password']):
            try : 
                user = User.objects.get(email=data['email'])
                user = authenticate(request, username=user.username, password=data['password'])
                if user is not None:
                    login(request, user)
                    return JsonResponse({
                        'status': 200,
                        'message': 'Succeessflly login'
                    })
                else:
                    return JsonResponse({
                        'status' : 401,
                        'message': 'Email / password not match, please check and try again. Or use google login if you register using google account'
                    })
            except User.DoesNotExist:
                return JsonResponse({
                    'status': 401,
                    'message': 'Email is not registered, please check and try again'
                })
    return HttpResponseBadRequest()

@csrf_exempt
def signup(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        if all(k in data for k in ['name', 'email', 'password', 'username']):
            try:
                # Check email duplicate
                user = User.objects.get(email = data['email']) 
                return JsonResponse({
                    'status': 409,
                    'message': 'Email already used. Please use another email and try again'
                })
            except User.DoesNotExist:
                try:
                    user = User.objects.get(username = data['username'])
                    return JsonResponse({
                        'status': 409,
                        'message': 'Username is used, please use another username'
                    })
                except User.DoesNotExist:
                    User.objects.create_user(data['username'], data['email'], data['password'], first_name = data['name'])
                    return JsonResponse({
                        'status': 200,
                        'message': 'Successfully create new account, please proced to login'
                    })
    return HttpResponseBadRequest()

@csrf_exempt
def g_login(request):  # pragma: no cover
    if request.method == 'POST':
        data = json.loads(request.body)
        print(data)
        if 'token' in data:
            try:
                idinfo = id_token.verify_oauth2_token(data['token'], requests.Request(), '48983791602-vf8qkmiadnnilkgkupk7mfqa94g8rjic.apps.googleusercontent.com')

                if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                    raise ValueError('Wrong issuer.')

                userid = idinfo['sub']
                email = idinfo['email']

                # Uername for django user , get email name
                username = email.split("@")[0]

                name = idinfo['name'].split(" ")
                first_name = name[0]
                last_name = " ".join(name[1:])

                # Check if user is exist
                try:
                    # Check if user ever login using google account
                    g_account = GoogleUserData.objects.get(google_id = userid)
                    login(request, g_account.user)
                except GoogleUserData.DoesNotExist:
                    try: 
                        # Try to find user with same email
                        user = User.objects.get(email=email)
                        GoogleUserData.objects.create(
                            google_id = userid,
                            user = user
                        )
                        login(request, user)
                    except User.DoesNotExist:
                        # If User havent login or have any account even with same email
                        user = User.objects.create_user(
                            username,
                            email,
                            password=None,
                            first_name=first_name,
                            last_name=last_name
                        )
                        GoogleUserData.objects.create(
                            user = user,
                            google_id = userid
                        )
                        login(request, user)
                return JsonResponse({
                    'status': 200,
                    'message': 'successfully login using google account'
                })
            except:
                return JsonResponse({
                    'status': 400,
                    'message': 'Failed with google verification'
                })
    return HttpResponseBadRequest()